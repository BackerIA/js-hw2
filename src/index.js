// 1.Які існують типи даних у Javascript?
//     Числа, строки, булеві.
// 2.У чому різниця між == і ===?
//     == - перевірка нестрогої рівності
//      5=='5'
//     == - строга рівність
//      5==5
// 3.Що таке оператор?
//     Це функція JS яка допомогає запускати код.
//     Для прикладу:
// - оператор рівності
// - оператор арифметичні
// - логічні
// -
// - ітд

let personalName;
let personalAge;
let personalBio;

personalName = prompt("Whats upp, who are u?"); // запитуєм користувача його ФІО
while (!/^[a-zA-z]+$/.test(personalName)) {
    personalName = prompt("Hm, are u kidding, its not you", personalName);
} //перевіряємо чи правлино він ввів ФІО

personalAge = prompt("How old are u bro?");
while (personalAge === 0 || Number(isNaN(personalAge))) {
    personalAge = prompt("How old are u bro?", personalAge);
} // перевірка введеня чисел

if (personalAge < 18) { // створюєм оператори перевірки if, else if,else
    alert("You are not allowed to visit this website"); // при відповіді менше 18 виводим You are not allowed to visit this website
} else if (personalAge >= 18 && personalAge <= 22) { // доп перевірка чи більше 18 та не менше ( включно ) 22
    personalBio = confirm("Are you sure you want to continue?"); //створення підтверждуючого вікна
    if (personalBio) { //якщо true - вивести привітання
        alert("Welcome " + personalName);
    } else { //якщо false - вивести заборону
        alert("You are not allowed to visit this website");
    }
} else { //якщо вік все інше ( більше 22 ) - вивести привітання
    alert("Welcome " + personalName);
}

